from flask import render_template, redirect, request, send_file, Response
from app import app
from app.models.entry import Entry
from app.colors import process_color
from gitlab import Gitlab
import anybadge
import requests
from io import BytesIO
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPM


@app.route("/")
def front_page():
    return render_template("index.html", title="Home")


@app.route("/favicon.ico")  # Some browsers make this request repeatedly and it throws errors
def favicon():
    return ""


@app.route("/badge/<badge_string>")
def badge_from_badge_string(badge_string):
    polished_params = []

    png = False
    if badge_string[-4:] == ".png":
        badge_string = badge_string[:-4]
        png = True

    current_param = ""
    last_was_dash = False
    for i in range(len(badge_string)):
        if badge_string[i] == "-":
            if last_was_dash:
                current_param += "-"
                last_was_dash = False
            else:
                last_was_dash = True
        else:
            if last_was_dash:
                polished_params.append(current_param)
                current_param = badge_string[i]
            else:
                current_param += badge_string[i]

            last_was_dash = False
    polished_params.append(current_param)

    for i in range(len(polished_params)):
        polished_params[i] = polished_params[i].strip("-")
        underscore_split = polished_params[i].split("__")
        for j in range(len(underscore_split)):
            underscore_split[j] = underscore_split[j].replace("_", " ")
        polished_params[i] = "_".join(underscore_split)

    while len(polished_params) < 3:
        polished_params.insert(0, "")

    label = polished_params[0]
    message = polished_params[1]
    color = polished_params[2]

    color = process_color(color)

    return gen_badge(label=label, value=message, default_color=color, format="png" if png else "svg")


@app.route("/badge/v1")
def badge_from_parameters():
    message = request.args.get("message")
    color = request.args.get("color")

    return gen_badge(**request.args)


@app.route("/gla/<username>")
def fetch_gitlab_avatar(username):
    gl = Gitlab()
    if username is not None and len(username) > 0:
        user_list = gl.users.list(username=username, get_all=False)

        if len(user_list) >= 1:
            return return_image_from_web("https://gitlab.com/uploads/-/system/user/avatar/" + str(user_list[0].id) +
                                         "/avatar.png", "image/png")
        else:
            group_list = gl.groups.list(search=username, get_all=True)

            for group in group_list:
                split_url = group.web_url.split("/")

                if (len(split_url) == 5) and ("/".join(split_url[4:]).lower() == username.lower()):
                    return return_image_from_web("https://gitlab.com/uploads/-/system/group/avatar/" + str(group.id) +
                                                 "/icon_mobile.png", "image/png")

            return send_file("../static/err-user-not-found.png", "image/png")
    else:
        return send_file("../static/err-user-not-found.png", "image/png")


@app.route("/<param>")
def process_url(param):  # put application's code here
    if len(param) > 3:
        return find_content(param)


def gen_badge(**kwargs):
    label = kwargs.get("label")
    message = kwargs.get("message")
    color = kwargs.get("color")
    img_format = kwargs.get("format")

    value = kwargs.get("value")
    if value is None:
        value = message

    default_color = kwargs.get("default_color")
    if default_color is None:
        default_color = color

    svg = str(anybadge.Badge(label=label, value=value, default_color=default_color))

    if img_format == "png":
        svgio = BytesIO(bytes(svg, "utf-8"))
        pngio = BytesIO()

        renderPM.drawToFile(svg2rlg(svgio), pngio, fmt="PNG")
        return Response(pngio.getvalue(), mimetype="image/png")
    else:
        return svg


def find_content(content_id):
    entry = Entry.query.get(content_id)

    if entry.type == "text/plain":
        return render_template("text_display.html", text=bytes.decode(entry.data))
    elif entry.type == "application/json":
        return render_template("json_display.html", json=bytes.decode(entry.data))
    elif entry.type[:6] == "image/":
        return Response(entry.data, mimetype=entry.type)
    elif entry.type[:9] == "redirect/":
        status_code = int(entry.type.split("/")[1])
        return redirect(bytes.decode(entry.data), code=status_code)


def return_image_from_web(url, mimetype):
    image = requests.get(url)

    if image.status_code % 100 == 2:
        return Response(image.content, mimetype=mimetype)
    else:
        return send_file("../static/err-no-image-found.png", "image/png")
