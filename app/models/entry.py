from app import db


class Entry(db.Model):
    id = db.Column(db.String, primary_key=True)
    type = db.Column(db.String)
    data = db.Column(db.LargeBinary)

    def __repr__(self):
        return f"<Entry {self.id} of type {self.type}>"
